'use strict';
const { role } = require('./../../src/api/models');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // const user = await queryInterface.rawSelect('User', {
    //   where: {
    //     name: 'John doe',
    //   },
    // }, ['id']);
    const roles = await role.findAll({ attributes: ['name'] });
    let myRoles = [{ "name": "PROFESSIONAL" }, { "name": "CLIENT" }];
    let filterMyRoles = myRoles.filter(f => !roles.map(m => m.name).includes(f.name));
    if (filterMyRoles.length > 0) {
      await queryInterface.bulkInsert('roles', filterMyRoles, {});
    }
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('roles', null, {});
  }
};
