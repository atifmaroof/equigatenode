
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const db = require("../models");
const constants = require('../../config/constants.js');
const Mail = require('../../middlewares/mailer');
const { generateToken, fourDigitToken } = require('../../middlewares/helper');
const { Op } = require('sequelize');

const UserController = {
    async signup(req, res) {
        console.log(req.body)
        try {
            let isEmailExists = await db.users.findOne({
                where: { email: req.body.email }
            })
            if (isEmailExists) {
                res.json({
                    success: false,
                    message: "Email already exits!"
                })
            }
            else {
                const hashedPassword = await bcrypt.hash(req.body.password, eval(10));
                req.body.password = hashedPassword;
                let token = fourDigitToken();
                req.body['verificationToken'] = token;
                Mail.sendEmail(["atif.maroof@gmail.com"], "New Registration", `Your Token Is ${token}`);
                const results = await db.users.create(req.body, {
                    include: {
                        model: db.userDetails,
                        as: 'userDetails',
                    },
                });
                res.json({
                    success: true,
                    id: results.id,
                })
            }
        } catch (e) {
            res.json({
                success: false,
                message: e.errors
            })
        }
    },
    async signin(req, res) {
        try {
            const user = await db.users.findOne({
                include: {
                    model: db.userDetails,
                    as: 'userDetails',
                },
                where: { email: req.body.email }
            });

            if (!user) {
                return res.status(401).json({ success: false, message: "Invalid Password", payload: {} });
            }

            const isSame = await bcrypt.compare(req.body.password, user.password);
            if (!isSame) {
                return res.status(401).json({ success: false, message: "Invalid Password", payload: {} });
            }

            const token = jwt.sign({ id: user.id, email: user.email, role_id: user.role_id }, "catchMeIfuCan", { expiresIn: 60 * 60 });
            return res.status(200).json({ success: true, message: constants.SuccessfullyLogin, payload: { token, user } });
        } catch (error) {
            res.json({
                success: false,
                message: error
            })
        }
    },
    getUsers(req, res) {
        db.users.findAll({
            include: {
                model: db.userDetails,
                as: 'userDetails',
            }
        }).then(users => res.json(users));
    },
    async getUserById(req, res) {
        // let mail = await Mail.sendEmail(["atif.maroof@gmail.com"], "New Registration", "<h1>hello there!</h1>");
        // let emailResponse = await mail.sendEmail(["atif.maroof@gmail.com"], "New Registration", "<h1>hello there!</h1>");
        // let toke = generateToken()
        // console.log(toke)

        db.users.findOne({
            where: { id: req.params.id },
            attributes: {
                exclude: ['createdAt', 'updatedAt', 'roleId', 'password']
            },
            include: [{
                model: db.role,
                as: 'role',
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                },
            }, {
                model: db.userDetails,
                as: 'userDetails',
            }],
        }).then(users => res.json(users));
    },
    async countUser(req, res) {
        console.log(req.params.id)
        db.users.count({
            where: { email: req.params.id }
        }).then(users => res.json(users));
    }
};

module.exports = UserController;