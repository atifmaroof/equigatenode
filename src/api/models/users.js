'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class users extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            users.hasOne(models.userDetails, { foreignKey: 'userId', as: 'userDetails', onDelete: 'CASCADE' });
            users.belongsTo(models.role, { as: 'role' });
        }
    };
    users.init({
        id: {
            primaryKey: true,
            allowNull: false,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        roleId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                customValidator(value) {
                    if (value !== 1 && value !== 2) {
                        throw new Error("Invalid Role Id");
                    }
                }
            }
        },
        isVerified: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        verificationToken: {
            type: DataTypes.STRING,
            allowNull: true
        },
    }, {
        sequelize,
        modelName: 'users',
    });
    // users.associate = function (models) {
    //     users.hasOne(models.userDetails, { foreignKey: 'userId', as: 'userDetails', onDelete: 'CASCADE' });
    // };
    return users;
};