
module.exports = (sequelize, DataTypes) => {
    const Post = sequelize.define("post", {
        id: {
            primaryKey: true,
            allowNull: false,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
    });
    // Post.associate = models => {
    //     Post.belongsTo(models.User, {
    //         allowNull: false
    //     })
    // }
    return Post;
}