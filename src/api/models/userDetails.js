
module.exports = (sequelize, DataTypes) => {
    let userDetails = sequelize.define("userDetails", {
        id: {
            primaryKey: true,
            allowNull: false,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
        },
        mobileNum: {
            type: DataTypes.STRING,
            allowNull: false
        },
        address: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        userId: DataTypes.UUID
    }, {
        tableName: 'user_details',
    });
    return userDetails;
}

// module.exports = (sequelize, DataTypes) => {
//     const userDetails = sequelize.define('userDetails', {
//         userId: DataTypes.INTEGER,
//         mobileNum: DataTypes.STRING,
//         address: DataTypes.TEXT,
//     }, {});
//     return userDetails;
// };
