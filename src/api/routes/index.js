
const express = require('express');
const app = express();
const UserRoute = require('./UserRoute');
const PostRoute = require('./PostRoute');
// const { auth } = require('../../middlewares')

app.get('/', (req, res) => { }).use('/user', UserRoute);
app.get('/', (req, res) => { }).use('/post', PostRoute);

module.exports = app;