const express = require('express');
const app = express();
const PostController = require('./../controller/PostController');
const auth = require('../../middlewares/auth')

app.get('/get', [auth], PostController.get);

module.exports = app;