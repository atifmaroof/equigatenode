const express = require('express');
const app = express();
const UserController = require('./../controller/UserController')

app.post('/signup', UserController.signup);
app.post('/signin', UserController.signin);
app.get('/getUsers', UserController.getUsers);
app.get('/getUserById/:id', UserController.getUserById);
app.get('/countUser/:id', UserController.countUser);

module.exports = app;