const generateToken = () => {
    let _ = Math.random().toString(36).substr(2)
    return (_ + _)
}

const fourDigitToken = () => {
    return Math.floor(1000 + Math.random() * 9000);
}
module.exports = {
    generateToken,
    fourDigitToken
};