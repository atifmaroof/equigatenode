const jwt = require('jsonwebtoken');

const auth = async (req, res, next) => {
    if (req.headers && req.headers['authorization']) {
        try {
            const token = req.headers['authorization'].split(" ")[1]?.trim()
            if (!token) {
                res.status(401).json({ success: false, message: "Invalid token or expired", payload: {} });
            }
            else {
                const decoded = jwt.verify(token, "catchMeIfuCan");
                req.user = decoded;
                next()
            }
        } catch (error) {
            return console.error(error), res.status(401).json({ success: false, message: "Invalid token or expired", payload: {} });
        }
    } else {
        return res.status(401).json({ success: false, message: "Token not provided", payload: {} });
    }
}
module.exports = auth;