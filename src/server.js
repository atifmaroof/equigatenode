const express = require("express");
const bodyParser = require('body-parser');
const Routes = require('./api/routes');
const db = require('./api/models');

let port = process.env.PORT || 3500;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api', Routes);

db.sequelize.sync({ force: false }).then(() => {
    app.listen(port, () => {
        console.log(`server running in ${port}`);
    });
})