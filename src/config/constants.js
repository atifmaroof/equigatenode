const constants = {
    DUPLICATE: 'ER_DUP_ENTRY',
    REGISTER_MSG_1: 'User successfully registered, please verify your email address',
    REGISTER_MSG_2: 'User already registered',
    VALIDATION_MSG: 'Validation error',
    SERVER_MSG: 'Server error',
    LOGIN_MSG_1: 'Invalid email or password',
    SuccessfullyLogin: 'Successfully Login',
    LOGIN_MSG_3: 'Please verify your email address',
    FORGOT_MSG_1: 'Successfully login',
    FORGOT_MSG_2: 'User not found with this email',
    RESET_MSG_1: 'Password has been reset successfully',
    RESET_MSG_2: 'User not found or token expired',
    ME_MSG: 'Auth user details',
    ADMIN_MSG_1: 'All users list'
}

module.exports = constants;