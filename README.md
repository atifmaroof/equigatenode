npx sequelize-cli model:generate --name role --attributes name:string
npx sequelize-cli migration:generate --name user-inactive-column
npx sequelize-cli db:migrate
npx sequelize-cli db:migrate:undo
npx sequelize-cli db:migrate:undo:all

npx sequelize-cli seed:generate --name role-add
npx sequelize-cli db:seed:all
npx sequelize-cli db:seed --seed 20211022121040-role-add.js
npx sequelize-cli db:seed:undo:all

# Sequelize Seed Production Database

https://www.youtube.com/watch?v=uESv9zpuVCE

# Disable Foreign key check

https://stackoverflow.com/questions/35771271/how-to-disable-all-foreign-keys-in-phpmyadmin-when-exporting-table-data
SET foreign_key_checks = 0;

# Gmail password 2 factor issue

https://stackoverflow.com/questions/60701936/error-invalid-login-application-specific-password-required

# ngrok

npm i g -s ngrok
https://blog.devgenius.io/giving-public-access-to-your-node-application-using-ngrok-3587c8064709
ngrok authtoken 1nKdrBHAo9NB0rkSDPeTioq64Ck_6QpekPXGU74j8QYNHqCir
ngrok http 3500
